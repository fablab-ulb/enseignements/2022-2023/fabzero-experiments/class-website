# ULB - How To Make (almost) Any Experiments / FabZero inside

We live in a very challenging time of rapid change and uncertainties. The United Nations have made a list of [17 Sustainable Development Goals](https://sdgs.un.org/) that we, as a world society should urgently act on in the next decade to make a more equitable and sustainable society.

There are many strategies that we can adopt and many ways our society can evolve. Rob Hopkins [^1] who has founded the transition town and Riel Miller [^2] at the UNESCO urge us to **dream, imagine, design and build the future we desire**.

In this class, you will take foot as a **social inventor and entrepreneur** working in **a team**. You will join an interdisciplinary community to **tackle a challenge you care about**.

![](./img/fablab-machine-logos.svg)

We will work at the crossroads of disciplines and community movements:

* **the digital fabrication revolution, the rise of fablabs and the maker movement**. In this class, we learn to use generic tools and workflows that are common in Fab  Labs connected to a wide interdisciplinary community and network. This allows us to develop and design projects globally and collaboratively and to fabricate locally.

* the **Frugal Science movement** which goal is to solve planetary scales problems using cost-effective scientifically based solutions that are scalable to meet the problem scale. You will learn the importance of basic science, tinkering and creative play to tackle design challenges.

* **the growth of creative, practicing and learning communities**, stimulated by **collective intelligence**, able to adapt, collaborate and solve problems. This class will mix undergrad students from different background that will team up with global collaborators and mentors all around the world to solve the identified challenges.

## Class scenario

In this class, you will start by **identifying a set of problems that you are passionate about** as an individual and as a team.

You will **learn organizational and managerial skills in collective intelligence** to solve problems, in interdisciplinary teams, that are bigger than you.

You will **learn to use digital fabrication tools** that you can find in a makerspace or a Fab Lab to build experiments or scientific tools.

Supported by mentors, in teams, you will **design, fabricate and document** a scientifically-based and frugal proof of concept of a project to solve the problem that you have identified as a team.

## Learning Community

### ULB students

[Virginie LOUCKX *](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx)  
[Victor DE PILLECYN *](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn)  
[Martin GILLES](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.martin)     
[Merlin BOSERET](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/boseret.merlin)  
[Benoît BURON](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/benoit.buron)  
[Donovan CROUSSE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/donovan.crousse)  
[Matthew DOYLE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle)  
[jules GERARD](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jules.gerard)  
[alicia GIMZA](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Alicia.gimza)  
[Thibault LEONARD](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/leonard.thibault)  
[Jean-François LOUMEAU](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jean-francois.loumeau)  
[Noé BOURGEOIS](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/noe.bourgeois)   
[Louis DEVROYE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/louis.devroye)  
[Thomas MARÉCHAL](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/thomas.marechal)  
[Stanislas MONDESIR](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/stanislas.mondesir)  
[Aymane TAIFOUR](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/aymane.taifour)  
[Cosmin TUDOR](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cosmin.tudor)  
[Louis VANSTAPPEN](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/louis.vanstappen)  
[Kodjo DAO](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kodjo.dao)  
[Emma DUBOIS BARKARDOTTIR](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/emma.dubois)  
[Sami EL HAMDOU](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/sami.el.hamdou)  
[Lucie LESTIENNE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Lucie.lestienne)  
[Suzanne LIPSKI](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski)  
[Mariam MEKRAY](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray)  
[Christophe ORY](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/christophe.ory)  
[Kamel SANOU](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou)  
[Kawtar ZAOUIA](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Kawtar.zaouia)  
[Ibrahima BAH](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/)  
[Lorea LATORRE MOLINA](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre)  
[Patrick DEZSÉRI](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/patrick.dezseri)  
[Halil-Ibrahim CELA](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/halil-ibrahim.cela)  
[Cédric LUPPENS](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cedric.luppens)  
[Dimitri DEBAUQUE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/dimitri.debauque)  
[Antoine TRILLET](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/antoine.trillet)  
[Chiara CASTRATARO](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/chiara.castrataro)  
[Karl PREUX](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/karl.preux)  
[Jonathan KAHAN](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/jonathan.kahan)  
[Gilles THEUNISSEN](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/gilles.theunissen)  
[Morgan TONGLET](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/morgan.tonglet)  
[Julien CALABRO](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/julien.calabro)  
[Eliot NIEDERCORN](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn)  
[Damien DELATTRE](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/damien.delattre)  
[Alexandre HALLEMANS](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/alexandre.hallemans)  


### FabZero-Experiments Teaching Designers and Coordinators

* Denis Terwagne, professor of physics and digital fabrication ([Fab Academy](http://archive.fabacademy.org/archives/2017/woma/students/238/), [Frugal Lab](http://frugal.ulb.be/), [FabLab ULB](http://fablab-ulb.be/))

with the help of :  

* Chloé Crokart, community facilitator  ([Collectiv-a](https://collectiv-a.be))
* Sophie Lecloux, pedagogical advisor ([CAP, ULB](https://www.ulb.be/fr/l-ulb-et-l-ecole/cap-centre-d-appui-pedagogique))

### FabZero Mentors

Fab Lab ULB is a community of researchers, teachers, technicians, students and citizens from all disciplines from all disciplines always ready to **volunteer** and **share** their knowledge and **mentor** students. We would like to thank :

* Nicolas De Coster, [IRM meteo](https://www.meteo.be/fr/bruxelles) scientist, scientific collaborator at [FabLab ULB](http://fablab-ulb.be/) ([Fab Academy](http://fab.academany.org/2018/labs/fablabulb/students/nicolas-decoster/))
* Axel Cornu, electronician at [Frugal Lab](https://frugal.ulb.be/) and [FabLab ULB](http://fablab-ulb.be/) ([Fab Academy](https://fabacademy.org/2019/labs/ulb/students/axel-cornu/about/index.html))
* Jonathan Vigne, expert repairer ([Repair Together](https://repairtogether.be/en/what-is-a-repair-cafe/))
* Pauline Mackelbert, project manager at STEAM Lab ([FabLab ULB](http://fablab-ulb.be/))
* Julien Van Den Hautte, intern @ [FabLab ULB](http://fablab-ulb.be/)
* Christophe Reyntiens, technician at EPB
* Colin Aughet, technician at EPB
* Simon Croegaert, technician at EPB
* Guillaume Deneyer, technician at EPB
* Gilles Decroly, biomechanical engineer at BAP, EPB
* Rami Taheri, Ph.D student at EPB

## Archives

* [2021-2022 ULB FabZero-Experiments Class Website](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/)
* [2020-2021 ULB FabZero-Experiments Class Website](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/)

## References

 [^1]: *From What Is to What If: Unleashing the Power of Imagination to Create the Future We Want*, Chelsea Green Publishing Co, 2019
 [^2]: Resilience Frontiers. Riel Miller. A futures Literacy Laboratory @UNESCO ([video](https://www.youtube.com/watch?v=_WgvTfR7TLI))
