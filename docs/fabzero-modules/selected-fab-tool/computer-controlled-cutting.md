# Computer Controlled Cutting

## Goal of this unit

In this unit, you will learn how to design, prepare 2D CAD files and lasercut them.

## Before class

* Install a 2D vector imagery tool, [Inkscape](https://inkscape.org/), and follow some [Inkscape basic tutorials](https://inkscape.org/learn/tutorials/)
* Read and study [this laser cutter manual](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut). You will be quizzed at the beginning of the class (particularly in the area of security) !


## Class materials

* [Laser cutter manual](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut)

## Assignment

Group assignment:

* characterize your lasercutter's focus, power, speed, kerf, ... for folding and cutting cardboard paper

Individual assignment:

* design, lasercut, and document a kirigami (cut and folds) to make a 3D object

## Learning outcome

* Demonstrate and describe 2D design processes
* Identify and explain processes involved in using the laser cutter.
* Develop, evaluate and construct the 3D object made from a kirigami
