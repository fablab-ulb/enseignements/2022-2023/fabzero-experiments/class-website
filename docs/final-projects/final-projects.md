# Final Projects

## Assignment

By groups, you will design, fabricate and document a proof of concept of a scientifically-based and frugal solution to solve a real-world problem you have identified.

* You will position your project along the [17 Sustainable Development Goals](https://sdgs.un.org/) listed by the United Nations.
* You will show scientific evidence with sources and references on which you build on your project.
* You will use at least one digital fabrication technique that is used in a fablab.
* You will explain the team dynamics and demonstrate how you worked as a team.

## Final projects documentation

| | |
| --- | --- |
|![Group 1 - RRRrrr!!!](img/group-01-slide.png) |**Group 1 - E-Waste** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-01/) <br> * [final pres.](img/group-01-presentation.pdf) |
|![Group 2 - Les héros de la Senne](img/group-02-slide.png) |**Group 2 - Les héros de la Senne** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-02/) <br> * [final pres.](img/group-02-presentation.pdf) |
|![Group 3 - L'équipe en carton](img/group-03-slide.png) |**Group 3 - L'équipe en carton** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-03/) <br> * [final pres.](img/group-03-presentation.pdf) |
|![Group 4 - Djiprobe](img/group-04-slide.png) |**Group 4 - Dembe** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-04/) <br> * [final pres.](img/group-04-presentation.pdf) |
|![Group 5 - Mushroom X](img/group-05-slide.png) |**Group 5 - Mushroom X** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-05/) <br> * [final pres.](img/group-05-presentation.pdf) |
|![Group 6](img/group-06-slide.png) |**Group 6 - A.L.E.D.** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-06/) <br> * [final pres.](img/group-06-presentation.pdf) |
|![Group 7](img/group-07-slide.png) |**Group 7 - Fabulous 7** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-07/) <br> * [final pres.](img/group-07-presentation.pdf) |
|![Group 8](img/group-08-slide.png) |**Group 8 - Ecobarista** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-08/) <br> * [final pres.](img/group-08-presentation.pdf) |
|![Group 9](img/group-09-slide.png) |**Group 9 - el nueve** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-09/) <br> * [final pres.](img/group-09-presentation.pdf) |
|![Group 10](img/group-10-slide.png) |**Group 10 - CafféineCheck** <br> * [project docs](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-10/) <br> * [final pres.](img/group-10-presentation.pdf) |


## Preparation for the final presentation

For the final presentation, you will prepare

* a graphical abstract illustrating your project (1920x1080px PNG image entitled "firstname.surname-slide.png", examples [here](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/class-website/final-projects/final-projects/) and [here](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/class-website/final-projects/final-projects/)) with :
    * a photo/image illustrating your project and prototype
    * [a logo of the FabLab ULB](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/class/-/tree/main/vade-mecum/images)
    * a short and clear question that your project tries to answer
* a pdf presentation (16 slides/8min) to share your project with an interdisciplinary audience ("firstname.surname-presentation.pdf").
* prototypes, proof of concept and technical/scientific documents that illustrate your process and your final achievement.
* a printed QR code linking to your online and finalized documentation
